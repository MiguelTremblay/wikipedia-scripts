# Wikipédia scripts

Scripts pour gérer différents projets sur Wikipédia.

Répertoire [Covid-19](./covid-19): scripts pour gérer la mise à jour des pages du projet [Pandémie de Covid-19 au Québec](https://fr.wikipedia.org/wiki/Pand%C3%A9mie_de_Covid-19_au_Qu%C3%A9bec).