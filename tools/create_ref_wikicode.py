#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Name:        create_ref_wikicode.py
Description: Génère le wikicode pour créer une référence à partir d'un hyperlien.
 Fonctione pour Wikipédia FR pour un article en particulier.

Média présentement géré par le script:
* Le Devoir

Notes:

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

Author: Miguel Tremblay
Date: 20 mars 2010


"""

import sys
import datetime

# URL manipulation
import urllib.request
from urllib.request import Request, urlopen
from urllib.parse import urlparse

REF_DEBIT = "<ref>{{Lien web"
REF_FIN = "}}</ref>"

l_media = {"Le Devoir": "ledevoir.com",
           "Le Journal de Montréal": "journaldemontreal.com",
           "Le Journal de Québec": "journaldequebec.com",
           "Le Soleil": "lesoleil.com",
           "La Tribune": "latribune.ca",
           "La Voix de l'Est": "lavoixdelest.ca",
           "Le Quotidien": "lequotidien.com",
           "La Presse": "lapresse.ca",
           "Le Droit": "ledroit.com",
           "Radio-Canada": "radio-canada.ca",
           "TVA Nouvelles": "tvanouvelles.ca",
           "Journal Métro": "journalmetro.com"}

l_cn2_i = ["Le Soleil", "La Tribune",
           "La Voix de l'Est", "Le Droit", "Le Quotidien"]


def main(s_ref_url):
    """
    s_ref_url est l'URL qu'on tente de référencer
    """

    # On regarde si ce site est supporté
    s_media = check_if_media_supported(s_ref_url)

    # Retire l'information d'identification à la fin de l'URL
    s_ref_url = s_ref_url.split("?")[0]

    create_wikicode(s_media, s_ref_url)


def removesuffix(s, suffix):
    """ Adapté de https://www.python.org/dev/peps/pep-0616/"""
    # suffix='' should not call self[:-0].
    if suffix and s.endswith(suffix):
        return s[:-len(suffix)]
    return s[:]


def titre_date_auteurs_ledevoir(s_le_devoir_url):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    fp = urllib.request.urlopen(s_le_devoir_url)
    mybytes = fp.read()

    mystr = mybytes.decode("utf8")
    fp.close()

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("headline")
    n_end = mystr[n_start:].find("\n")
    s_line = mystr[n_start:n_start+n_end]

    # On fait du nettoyage
    s_title_ligne = s_line.split(":", 1)[1].strip('"')
    n_start2 = s_title_ligne.find('"')+1
    n_end2 = s_title_ligne.rfind('"')
    s_titre = s_title_ligne[n_start2:n_end2]

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("datetime")
    n_end = mystr[n_start:].find("\n")
    s_line = mystr[n_start:n_start+n_end]
    n_start2 = s_line.find('>')+1
    n_end2 = s_line.rfind('<')
    s_date_publication = s_line[n_start2:n_end2]

    # Auteur de l'article
    # On cherche la valeur après "datetime"
    l_auteurs = []
    n_start = mystr.find("articleAuthor")
    if n_start < 0:  # Le Devoir semble avoir changé son HTML, quelque part en janvier 2021
        n_start = mystr.find('"author"')
        if n_start < 0:  # Dans le cas des lettres d'opinion,
            #  l'auteur n'est pas indiqué avec des métadonnées appropriées
            print(
                "\n === \n Page opinion du Devoir. Ajouter le nom de l'auteur à bras.\n === \n")
            return (s_titre, s_date_publication, l_auteurs)
        n_end = mystr[n_start:].find("}")
        s_ligne = mystr[n_start:n_start+n_end]
        n_start2 = s_ligne.find('"name": "') + len('"name": "')
        n_end2 = s_ligne[n_start2:].rfind('"')
        s_ligne2 = s_ligne[n_start2:n_start2+n_end2]
        l_ligne = s_ligne2.split(",")
    else:
        n_end = mystr[n_start:].find("\n")
        s_ligne = mystr[n_start:n_start+n_end]
        l_ligne = s_ligne.split(':', 1)[1].split("'")[1].split(",")

    for i, _ in enumerate(l_ligne):
        # for i in range(len(l_ligne)):
        l_auteurs.append(l_ligne[i].strip())

    return (s_titre, s_date_publication, l_auteurs)


def titre_date_auteurs__jd_m(s_jd_murl):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    fp = urllib.request.urlopen(s_jd_murl)
    mybytes = fp.read()

    mystr = mybytes.decode("utf8")
    fp.close()

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("<title>") + len("<title>")
    n_end = mystr[n_start:].find("</title>") - len(" | JDM")
    s_titre = mystr[n_start:n_start+n_end]

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("datetime") + len('datetime="')
    n_end = mystr[n_start:].find("\n") - len('THH:MM:SSZ"')
    s_date_publication = mystr[n_start:n_start+n_end]

    # Auteur de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("articleAuthor")
    n_end = mystr[n_start:].find("\n")
    s_line = mystr[n_start:n_start+n_end]

    l_ligne = s_line.split(':', 1)[1].split('"')[1].split(",")
    l_auteurs = []
    for i, _ in enumerate(l_ligne):
        l_auteurs.append(l_ligne[i].strip())

    return (s_titre, s_date_publication, l_auteurs)


def titre_date_auteurs_cn2i(s_cn2i_url):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    fp = urllib.request.urlopen(s_cn2i_url)
    mybytes = fp.read()
    mystr = mybytes.decode("utf8")
    fp.close()

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("<title>") + len("<title>")
    n_end = mystr[n_start:].find("|") - 1
    s_titre = mystr[n_start:n_start+n_end]

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.rfind("datetime") + len('datetime="')
    n_end = n_start + 10  # 10 is the length of YYYY-MM-DD
    s_date_publication = mystr[n_start:n_end]

    # Auteur de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("full-article-basic-author__info-name")
    n_fin_nom = mystr[n_start:].find(">") + 1
    n_start = n_start + n_fin_nom
    n_end = mystr[n_start:].find("<")
    s_line = mystr[n_start:n_start+n_end]

    l_auteurs = []
    l_auteurs.append(s_line.strip())

    return (s_titre, s_date_publication, l_auteurs)


def titre_date_auteurs__la_presse(s_la_presse_url):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    # La Presse n'aime pas les robots! ON lui fait accroire qu'on est un dinosaure!
    req = Request(s_la_presse_url, headers={'User-Agent': 'Mozilla/5.0'})
    mybytes = urlopen(req).read()
    mystr = mybytes.decode("utf8")

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("<title>") + len("<title>")
    n_end = mystr[n_start:].find("<")
    s_titre = mystr[n_start:n_start+n_end]
    #print (s_titre)
    s_titre = removesuffix(s_titre, "| La Presse")
    s_titre = removesuffix(s_titre, " - La Presse+")
    s_titre = s_titre.replace("|", "{{!}}")
    #print (s_titre)

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("datetime")
    if n_start < 0:  # Essaye publicationDate
        n_start = mystr.find("publicationDate") + len('publicationDate":"')
        print(n_start)
    else:
        n_start = n_start + len('datetime="')
    n_end = n_start + 10  # 10 is the length of YYYY-MM-DD
    s_date_publication = mystr[n_start:n_end]

    # Auteur de l'article
    # On cherche la valeur après "datetime"
    n_length = len("name authorModule__name") + 3
    n_start = mystr.find("name authorModule__name") + n_length
    if n_start < n_length:
        n_start = mystr.find("organization authorModule__organisation") + \
            len("organization authorModule__organisation") + 3

    n_end = mystr[n_start:].find("<")
    s_line = mystr[n_start:n_start+n_end]

    l_auteurs = []
    l_auteurs.append(s_line.strip())

    return (s_titre, s_date_publication, l_auteurs)


def titre_date_auteurs__radio_canada(s_radio_canada_url):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    req = Request(s_radio_canada_url)
    mybytes = urlopen(req).read()
    mystr = mybytes.decode("utf8")

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("<title>") + len("<title>")
    # Des fois le pipe est là, d'autre fois non comme sur Ohdio.
    n_end1 = mystr[n_start:].find("|")
    n_end2 = mystr[n_start:].find("</title>")
    if n_end1 < n_end2:
        n_end = n_end1
    else:
        n_end = n_end2
    s_titre = mystr[n_start:n_start+n_end]
    s_titre = s_titre.strip()
    s_titre = s_titre.replace("|", "{{!}}")

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("datePublished")
    if n_start > 0:
        n_start += len('datePublished": "')
        n_end = n_start + 10  # 10 is the length of YYYY-MM-DD
        s_date_publication = mystr[n_start:n_end]
    else:
        s_date_publication = "DATE INCONNUE: METTRE À BRAS"

    # Auteur de l'article
    n_start = mystr.find('"name": "')

    if n_start > 0:
        n_start += len('"name": "')
        n_end = mystr[n_start:].find('"')
        s_line = mystr[n_start:n_start+n_end]
    else:
        s_line = "Radio-Canada"

    l_auteurs = []
    l_auteurs.append(s_line.strip())

    return (s_titre, s_date_publication, l_auteurs)


def titre_date_auteurs_tva(s_tva_url):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    fp = urllib.request.urlopen(s_tva_url)
    mybytes = fp.read()

    mystr = mybytes.decode("utf8")
    fp.close()

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("<title>") + len("<title>")
    n_end = mystr[n_start:].find("</title>") - len(" | TVA Nouvelles")
    s_titre = mystr[n_start:n_start+n_end]

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("datetime") + len('datetime="')
    n_end = len('YYYY-MM-DD')
    s_date_publication = mystr[n_start:n_start+n_end]

    # Auteur de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("cXenseParse:author")
    n_end = mystr[n_start:].find("\n")
    s_line = mystr[n_start:n_start+n_end]

    l_ligne = s_line.split('"')[2]
    l_auteurs = [l_ligne]

    return (s_titre, s_date_publication, l_auteurs)


def titre_date_auteurs__metro(s_metro_url):
    """
    Les contrôles sont effectués, reste à générer le wikicode.
    """

    fp = urllib.request.urlopen(s_metro_url)
    mybytes = fp.read()

    mystr = mybytes.decode("utf8")
    fp.close()

    # Titre
    # On cherche la valeur après "headline"
    n_start = mystr.find("<title>") + len("<title>")
    n_end = mystr[n_start:].find("</title>")
    s_titre = mystr[n_start:n_start+n_end]

    # Date de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find("datePublished") + len('datePublished\":\"')
    n_end = len('YYYY-MM-DD')
    s_date_publication = mystr[n_start:n_start+n_end]

    # Auteur de l'article
    # On cherche la valeur après "datetime"
    n_start = mystr.find('<meta name="author" content="') + \
        len('<meta name="author" content="')
    n_end = mystr[n_start:].find("\n") - 2
    s_line = mystr[n_start:n_start+n_end]

    l_auteurs = [s_line]

    return (s_titre, s_date_publication, l_auteurs)


def insert_attribute(s_titre_article, s_date_article, l_auteurs_article, s_nom_journal, s_url):
    """
    Insérer les attributs demandés dans le wikicode de la référence.

    Le format de la référence est
     "<ref>{{Lien web|url=<!-- Paramètre obligatoire -->
     |titre=<!-- Paramètre obligatoire -->
     |site= |prénom1= |nom1= | date= |consulté le= 2 octobre 2020 }}</ref>"

    """

    # URL
    s_total = REF_DEBIT + " |url=" + s_url
    # Titre
    s_total = s_total + " |titre=" + s_titre_article
    # Media
    s_total = s_total + " |site=" + s_nom_journal

    # Auteurs
    for i, _ in enumerate(l_auteurs_article):

        s_auteur = l_auteurs_article[i]
        l_prenom_nom = s_auteur.split()
        s_prenom = l_prenom_nom[0]

        # Cas spécial pour TVA Nouvelles
        if s_prenom == "TVA":
            s_nom = l_prenom_nom[1]
            s_total = s_total + " |auteur" + \
                str(i+1) + "=" + s_prenom + " " + s_nom
        # Lorsque Radio-Canada n'indique pas d'auteur
        elif s_prenom == "Radio-Canada":
            s_total = s_total + " |auteur=Radio-Canada"
        elif s_auteur == "La Presse canadienne":
            s_total = s_total + " |auteur=La Presse Canadienne"
        elif s_auteur == "AFP":
            s_total = s_total + " |auteur=Agence France-Presse"
        else:
            s_nom = l_prenom_nom[1]
            s_total = s_total + " |prénom" + str(i+1) + "=" + s_prenom
            s_total = s_total + " |nom" + str(i+1) + "=" + s_nom

    # Date
    s_total = s_total + " |date=" + s_date_article
    # Consulté en date d'aujourd'hui
    s_total = s_total + " |consulté le=" + \
        datetime.datetime.now().isoformat()[0:10]

    s_total = s_total + REF_FIN

    return s_total


def create_wikicode(s_media_name, s_media_url):
    """
    Switch case pour chaque media.
    """

    if s_media_name == "Le Devoir":
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs_ledevoir(s_media_url)
    elif s_media_name in ("Le Journal de Montréal", "Le Journal de Québec"):
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs__jd_m(s_media_url)
    elif s_media_name in l_cn2_i:
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs_cn2i(s_media_url)
    elif s_media_name == "La Presse":
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs__la_presse(s_media_url)
    elif s_media_name == "Radio-Canada":
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs__radio_canada(s_media_url)
    elif s_media_name == "TVA Nouvelles":
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs_tva(s_media_url)
    elif s_media_name == "Journal Métro":
        (s_titre, s_date, l_auteurs_art) = titre_date_auteurs__metro(s_media_url)

    s_wikicode = insert_attribute(
        s_titre, s_date, l_auteurs_art, s_media_name, s_media_url)

    print(s_wikicode)


def check_if_media_supported(s_media_url):
    """
    Vérification si l'URL concerne un média qui est supporté.
    """

    url_parsed = urlparse(s_media_url)
    s_domain = url_parsed.netloc

    for s_key, s_supported_media_url in l_media.items():
        if s_supported_media_url in s_domain:
            return s_key

    # Media non supporté
    print("Ce site n'est pas supporté par ce script:", s_domain)
    sys.exit(101)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage create_ref_wikicode.py URL")
        sys.exit(1)

    s_url_main = sys.argv[1]

    main(s_url_main)
