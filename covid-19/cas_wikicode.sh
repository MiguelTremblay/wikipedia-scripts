#!/usr/bin/env bash 
# title          : deces_wikicode.sh
# description     : Charge le fichier CSV découpé de l'INSPQ qui ne contient que les données pour le 
#                   Québec. Fait les manipulations nécessaires pour appeler créer le wikicode pour
#                    les graphiques des décès.
# author	  : Miguel Tremblay https://ptaff.ca/miguel/
# date            :14 octobre 2020
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


# Entrée: CSV avec les colonnes suivantes:
# 1- Date format 'YYYY-MM-DD'
# 2- Cas quotidien
# 3- Cumulatif des cas
# 4- Rétablis quotidien
# 5- Rétablis totaux
# 6- Décès quotidien
# 7- Décès totaux
# 8- Décès CHSLD quotidien 
# 9- Décès CHSLD totaux

EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
 echo "Usage: `basename $0` qc_total.csv"
 exit $E_BADARGS
fi

source static_wikicode.sh

FICHIER_CSV=$1

HEADER_CAS_QUOTIDIEN="${HEADER}
| yTitle=Cas quotidien
| yAxisTitle=Nombre de cas"

HEADER_DECES_TOTAUX="${HEADER}
| yTitle=Cas totaux
| yAxisTitle=Nombre de cas"

nbr_ligne=`wc -l $FICHIER_CSV | awk '{print $1}'`

# Les données débutent à partir de la 5e ligne. 
lDateISO=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $1","}' | xargs`
lCasQuotidien=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $2","}' | xargs`
lCasTotaux=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $3","}' | xargs`

echo "== Cas =="
echo ""
echo "=== Cas quotidiens ==="
echo ""
sWikicodeQuotidien="${HEADER_CAS_QUOTIDIEN}
| x=${lDateISO}
| y=${lCasQuotidien}
${FOOTER}"
echo "$sWikicodeQuotidien"

echo "=== Cas totaux ==="
echo ""
sWikicodeTotaux="${HEADER_DECES_TOTAUX}
| x=${lDateISO}
| y=${lCasTotaux}
${FOOTER}"
echo "$sWikicodeTotaux"
