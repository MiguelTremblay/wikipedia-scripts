#!/bin/bash
# title          : modele_wikicode.sh
# description     : Charge le fichier CSV découpé de l'INSPQ qui ne contient que les données pour le 
#                   Québec. Fait les manipulations nécessaires pour appeler convert_wikipedia_covid-19.sh
#                   avec les bons arguments.
# author	  : Miguel Tremblay https://ptaff.ca/miguel/
# date            :8 octobre 2020
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


# Les notes

EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -lt $EXPECTED_ARGS ]
then
 echo "Usage: `basename $0` qc_total.csv" 
 exit $E_BADARGS
fi


source static_wikicode.sh

echo "$HEADER_JSON"

mlr \
        --icsv      \
        --ojson  \
        --jlistwrap \
        cut \
                -f Date,dec_cum_tot_n,ret_cum_tot_n,cas_cum_tot_n \
                $1 \
                | jq '
                        {
                                data: [
                                        .[]
                                        |
                                        [ .Date, .dec_cum_tot_n, .ret_cum_tot_n, .cas_cum_tot_n ]
                                ]
                        }
                ' | tail -n +2  # Tail pour retirer le "{" en extra. L'accolade d'ouverture du fichier est dans le header.


