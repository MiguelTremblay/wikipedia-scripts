#!/bin/bash

TMP="/tmp"

# Aujourd'hui en format YYYY-MM-DD
today=`date +%Y-%m-%d`
displayAfter=$(date -d 2020-03-18 +%s)

# URL des fichiers et colonnes associées ## Fichier principal
INSPQ_CSV_URL="https://inspq.qc.ca/sites/default/files/covid/donnees/covid19-hist.csv"
# Colonnes pour le QC en entier
# QC_TOTAL_COLUMNS="1,8,4,12,11,19,13"
# 1 = Date
# 12 = Cas totaux QUOTIDIEN (cas_quo_tot_n)
# 7 = Cas totaux CUMUL (cas_cum_tot_n)
# 18 = Rétablis QUOTIDIEN (ret_quo_tot_n)
# 17 -> 15 = Rétablis CUMUL (ret_cum_tot_n)
# 21 -> 26 -> 24 = Décès totaux QUOTIDIEN (dec_quo_tot_n)
# 19 -> 17 = Décès totaux CUMUL (dec_cum_tot_n)
# 22 -> 27 -> 25= Décès quotidien CHSLD (dec_quo_chs_n)
# 27 -> 22 -> 20 = Décès totaux CHSLD (dec_cum_chs_n)
# 42 -> 43 -> 41 = Hospitalisation hors soins intentifs (nouvelle définition, hos_act_reg_n)
# 43 -> 44 -> 42 = Hospitalisation soins intensifs (hos_act_si_n)
QC_TOTAL_COLUMNS="1,12,7,18,15,24,17,25,20,41,42"
QC_TOTAL_CSV="qc_total.csv"

## Hospitalisations
INSPQ_CSV_HOSP_URL="https://inspq.qc.ca/sites/default/files/covid/donnees/manual-data.csv"
INSPQ_TMP_FILE="${TMP}/covid19-hist.csv"
# 1: Date
# 2: hospitalisations (nouvelle définition)
# 4: hospitalisations (ancienne définition)
# 3: soins intensifs
# 5: nombre de tests
HOSP_COLUMNS="1,2,4,3,5"

## Vaccination
INSPQ_CSV_VACCIN="https://inspq.qc.ca/sites/default/files/covid/donnees/vaccination.csv"
VACCIN_COLUMNS="1,5,6,7,8,13,9,10,11,12,14,15,16"
# 1: Date

# 5:  Doses quotidiennes 1ere dose (vac_quo_1_n)
# 6: Doses quotidiennes 2e dose (vac_quo_2_n)
# 7: Doses quotidiennes 3e dose (vac_quo_3_n)
# 8: Doses quotidiennes 3e dose (vac_quo_4_n)
# 11 -> 13:  Doses quotidiennes total (vac_quo_tot_n)

# 8 -> 9 : Doses cumul 1ere dose (vac_cum_1_n)
# 9 -> 10 :  Doses cumul 2e dose (vac_cum_2_n)
# 10 -> 11:  Doses cumul 3e dose (vac_cum_3_n)
# 11 -> 12:  Doses cumul 4e dose (vac_cum_4_n)
# 12 -> 14 : Doses cumulatives totales (vac_cum_tot_n)


# 13 -> 15:  % de la population avec vaccination de base complétée (cvac_comp_tot_p) 
# 14 -> 16:  % Proportion de la population ayant reçu au moins une dose de rappel parmi la population ayant une vaccination de base complétée au 15 août 2022, selon le groupe d’âge et selon le groupe prioritaire(cvac_cum_tot_2_p)



HEADER=$'{{Graph:Chart
| height =
| width = 800
| xAxisTitle=Date (YYYY-MM)
| xType = date
| xAxisAngle=-40
| xAxisFormat=%Y-%m
| yGrid= 1
| xGrid= 1
| colors= #ff8000, #f0be8b, '

HEADER_VACC=$'{{Graph:Chart
| height =
| width = 800
| xAxisTitle=Date (YYYY-MM)
| xType = date
| xAxisAngle=-40
| xAxisFormat=%Y-%m
| yGrid= 1
| xGrid= 1
| colors= #A7D9D5, #009E9E, #006161, #002222, #808080
| legend=Légende'

FOOTER=$"}}<div style=\"font-size:80%; line-height:1.2em;\">
* Source: [$INSPQ_CSV_URL Fichier CSV] sur le site de l'[https://www.inspq.qc.ca/covid-19/donnees INSPQ] récupéré en date du $today.</div>

"

FOOTER_HOSP=$"}}<div style=\"font-size:80%; line-height:1.2em;\">
* Sources: Fichiers CSV pour [$INSPQ_CSV_HOSP_URL les anciennes définitions] et [$INSPQ_CSV_URL les nouvelles définitions] d'hospitalisation sur le site de l'[https://www.inspq.qc.ca/covid-19/donnees INSPQ] récupéré en date du $today.
* À partir du {{date-|19 mai 2020}}, seuls les hôpitaux de soins généraux et spécialisés offrant des soins aigus sont considérés. Les centres hospitaliers dédiés à la santé mentale ou offrant principalement des soins de gériatrie, réadaptation et convalescence ont été retirés du décompte des hospitalisations, à l’exception du centre de services pour les aînés de Saint-Lambert qui a continué d’offrir des soins aigus jusqu’au 1er septembre 2020<ref>{{Lien web| url=https://www.inspq.qc.ca/covid-19/donnees/methodologie |titre=Méthodologie des données COVID-19  |site=INSPQ | date=15 avril 2021 |consulté le= 17 mai 2021 }}</ref>. 
</div>

"

FOOTER_SI=$"}}<div style=\"font-size:80%; line-height:1.2em;\">
* Source: [$INSPQ_CSV_HOSP_URL Fichier CSV] sur le site de l'[https://www.inspq.qc.ca/covid-19/donnees INSPQ] récupéré en date du $today.</div>

"

FOOTER_VACC=$"}}<div style=\"font-size:80%; line-height:1.2em;\">
* Source: [$INSPQ_CSV_VACCIN Fichier CSV] sur le site de l'[https://www.inspq.qc.ca/covid-19/donnees/vaccination INSPQ] récupéré en date du $today.</div>

"



HEADER_JSON=$'{
    "license": "CC0-1.0",
    "description": {
        "en": "Québec Covid-19 data from INSPQ",
        "fr": "Données de la Covid-19 du Québec de l'"'"'INSPQ"
    },
    "sources": "Source: [https://inspq.qc.ca/sites/default/files/covid/donnees/covid19-hist.csv Fichier CSV] sur le site de l'"'"'[https://www.inspq.qc.ca/covid-19/donnees INSPQ]",
    "schema": {
        "fields": [
            {
                "name": "date",
                "type": "string",
                "title": {
                    "en": "Date",
                    "fr": "Date"
                }
            },
            {
                "name": "deathstotal",
                "type": "number",
                "title": {
                    "en": "Deaths total",
                    "fr": "Total des décès"
                }
            },
            {
                "name": "recoveriestotal",
                "type": "number",
                "title": {
                    "en": "Recoveries total",
                    "fr": "Total des guérisons"
                }
            },
            {
                "name": "confirmedcases",
                "type": "number",
                "title": {
                    "en": "Confirmed cases",
                    "fr": "Cas confirmés"
                }
            }
        ]
    },
'
