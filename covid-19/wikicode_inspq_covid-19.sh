#!/bin/bash -       
#
# title           : wikicode_inspq_covid-19.sh
# description     :Découper le fichier CSV dans ses différents parties
# author		 : Miguel Tremblay https://ptaff.ca/miguel/
# date            :8 octobre 2020
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

# Chargement des variables, dont les URL de l'INSPQ
source static_wikicode.sh

declare -A REGIONCODE=( 
   ["01"]="Bas-Saint-Laurent"
   ["02"]="Saguenay–Lac-Saint-Jean"
   ["03"]="Capitale-Nationale"
   ["04"]="Mauricie et Centre-du-Québec"
   ["05"]="Estrie"
   ["06"]="Montréal"
   ["07"]="Outaouais"
   ["08"]="Abitibi-Témiscamingue"
   ["09"]="Côte-Nord"
   ["10"]="Nord-du-Québec"
   ["11"]="Gaspésie–Îles-de-la-Madeleine"
   ["12"]="Chaudière-Appalaches"
   ["13"]="Laval"
   ["14"]="Lanaudière"
   ["15"]="Laurentides"
   ["16"]="Montérégie"
   ["17"]="Nunavik"
   ["18"]="Terres-Cries-de-la-Baie-James"
   ["99"]="Québec au complet")



while getopts "R:M:o:mdHDchv" opt; do
  case $opt in
    m) # wikicode pour le modèle pour toutes les dates
      MODELE="true"
      sDate=""
      echo "-m données pour les modèles (complet)"
      ;;
    M) # wikicode pour le modèle à partir d'une date
      MODELE="true"
      sDate=$OPTARG
      echo "-M données pour les modèles (à partir de: $OPTARG)" >&2
      ;; 
    o) # Fichier de sortie pour le wikicode. Obligatoire
      sOUTPUT=$OPTARG
      echo "-o données seront mises dans le fichier: $OPTARG" >&2
      ;;
    d) # Graphiques pour les décès
      DECES="true"
      echo "-d pour les décès demandé"
      ;;
    H) # Graphique pour les utilisations
      HOSPITALISATION="true"
      echo "-H pour les hospitalisations demandé"
      ;;
    D) # Graphique pour les dépistages
      DEPISTAGE="true"
      echo "-D pour les dépistages"
      ;;
    c) # Graphiques pour les cas
      CAS="true"
      echo "-c pour les cas"
      ;;
    R) # Région 
      REGION=$OPTARG
      echo "-R pour la région $OPTARG" >&2
      ;;      
    v) # vaccinnation
      VACCIN="true"
      echo "-v pour les données sur la vaccination $OPTARG" >&2
      ;;            
    h) # Graphiques pour les cas
      echo "-h aide"
      echo "-o wikicode dans [fichier]" >&2
      echo "-m modèle (json pour Commons)"
      echo "-d décès"
      echo "-H hospitalisations (QC complet seulement)"
      echo "-D dépistages (QC complet seulement)"
      echo "-c cas"
      echo "-v vaccination"
      echo "-R REGION. Codes:"
      for i in `seq -w 18`; do
         echo "   $i: ${REGIONCODE[$i]}"
      done
      echo "   99: ${REGIONCODE[99]}"
      exit 0
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      ;;
  esac
done


# Vérifie s'il y a un fichier local
if [ -z ${sOUTPUT+x} ]; then 
   echo "Aucun fichier de sortie spécifié. En mettre un en utilisant l'option -o"
   exit 233
elif [ -f $sOUTPUT ]; then
   rm -fv $sOUTPUT
fi

# # Code de région
if [ -z ${REGION+x} ]; then 
   echo "Aucune région demandée, utilisation du Québec au complet (99)"
   REGION=99
fi
CLE_REGION="RSS$REGION"


######### Traitement CSV principal ####################

# Processing du CSV principal de l'INSPQ
if [ "$MODELE" = true ] || [ "$CAS" = true ] || [ "$DECES" = true ] ||  [ "$HOSPITALISATION" = true ] ; then
   
      # Vérifie s'il y a un fichier CSV de l'INSPQ local 
   if [ -z ${INSPQ_PATH+x} ]; then    
      # Pas de fichier local donné en ligne de commande. On regarde si la version existe dans /tmp
      echo $INSPQ_TMP_FILE
      if [ -f  $INSPQ_TMP_FILE ]; then
         echo -n "Fichier principal INSPQ existant dans $TMP "
         # la version existe, est-elle plus vieille que 24h
         HR24=`find  $TMP -maxdepth 1 -name "covid19-hist.csv" -mtime -1`
         if [ -z ${HR24} ]; then
            echo  "mais plus vieux que 24 hr. On télécharge "
            wget -N $INSPQ_CSV_URL?prout=$( date +"%s" ) -O $TMP/covid19-hist.csv && INSPQ_PATH=$INSPQ_TMP_FILE
         else
            echo "et plus récent que 24 hr. On va travailler avec cette version."
            INSPQ_PATH=$INSPQ_TMP_FILE
         fi
      else
         # On télécharge le fichier
         wget $INSPQ_CSV_URL?prout=$( date +"%s" ) -O $TMP/covid19-hist.csv && INSPQ_PATH=$INSPQ_TMP_FILE
      fi
   else 
      echo "var is set to '$INSPQ_PATH'"; 
   fi
   
   # Entête du CSV
   head -n1 $INSPQ_PATH > $TMP/qc_tmp.csv

   grep $CLE_REGION $INSPQ_PATH >> $TMP/qc_tmp.csv
   csvcut -c $QC_TOTAL_COLUMNS  $TMP/qc_tmp.csv > $TMP/$QC_TOTAL_CSV
fi



# Le modèle
if [ "$MODELE" = true ] ; then
  # Le modèle sur Commons n'aime pas la "Date inconnue", on retire la ligne
  sed "/Date\ inconnue/d" $TMP/$QC_TOTAL_CSV > $TMP/qc_total_modele.csv
   ./modele_wikicode.sh $TMP/qc_total_modele.csv   >> $sOUTPUT
fi

# Les cas
   if [ "$CAS" = true ]  ; then
   ./cas_wikicode.sh $TMP/$QC_TOTAL_CSV >> $sOUTPUT
fi

# Les décès
if [ "$DECES" = true ] ; then
   ./deces_wikicode.sh $TMP/$QC_TOTAL_CSV >> $sOUTPUT
fi

# Hospitalisation et dépistage, dans un autre fichier
if [ "$HOSPITALISATION" = true ] || [ "$DEPISTAGE" = true ] && [ "$REGION" = 99 ]; then
 
  INSPQ_TMP_FILE="$TMP/manual-data.csv" 
   echo $INSPQ_TMP_FILE
   if [ -f  $INSPQ_TMP_FILE ]; then
         echo -n "Fichier hospitalisation/dépistage existant dans $TMP "
   fi
   HR24=`find  $TMP -maxdepth 1 -name "manual-data.csv" -mtime -1`
   if [ -z ${HR24} ]; then
      echo  "mais plus vieux que 24 hr. On télécharge "
      wget -N $INSPQ_CSV_HOSP_URL?prout=$( date +"%s" ) -O $TMP/manual-data.csv && INSPQ_HOSP_PATH="$TMP/manual-data.csv" 
   else
      echo "et plus récent que 24 hr. On va travailler avec cette version."
      INSPQ_HOSP_PATH=$INSPQ_TMP_FILE
   fi

   # On enlève les 23 premières lignes 
   sed -n '24,$p' $INSPQ_HOSP_PATH > $TMP/hosp_tmp.csv 
   csvcut -c $HOSP_COLUMNS  $TMP/hosp_tmp.csv > $TMP/hosp.csv
 
 
   if [ "$HOSPITALISATION" = true ]; then
      ./hospitalisation_wikicode.sh $TMP/hosp.csv $TMP/$QC_TOTAL_CSV >> $sOUTPUT
   fi
   if  [ "$DEPISTAGE" = true ]; then
      ./depistage_wikicode.sh $TMP/hosp.csv >> $sOUTPUT
   fi
fi


# Vaccination
if [ "$VACCIN" = true ]; then

  INSPQ_TMP_FILE="$TMP/vaccination.csv" 
   echo $INSPQ_TMP_FILE
   if [ -f  $INSPQ_TMP_FILE ]; then
         echo -n "Fichier vaccination existant dans $TMP "
   fi
   HR24=`find  $TMP -maxdepth 1 -name "vaccination.csv" -mtime -1`
   if [ -z ${HR24} ]; then
      echo  "mais plus vieux que 24 hr. On télécharge "
      wget -N $INSPQ_CSV_VACCIN?prout=$( date +"%s" ) -O $TMP/vaccination.csv && INSPQ_VACCIN_PATH="$TMP/vaccination.csv" 
   else
      echo "et plus récent que 24 hr. On va travailler avec cette version."
      INSPQ_VACCIN_PATH=$INSPQ_TMP_FILE
   fi

   grep $CLE_REGION  $INSPQ_VACCIN_PATH > $TMP/vaccin_tmp.csv
   csvcut -c $VACCIN_COLUMNS  $TMP/vaccin_tmp.csv > $TMP/vacc.csv
   ./vaccin_wikicode.sh $TMP/vacc.csv >> $sOUTPUT
fi
