#!/usr/bin/env bash 
# title          : deces_wikicode.sh
# description     : Charge le fichier CSV découpé de l'INSPQ qui ne contient que les données pour le 
#                   Québec. Fait les manipulations nécessaires pour appeler créer le wikicode pour
#                    les graphiques des décès.
# author	  : Miguel Tremblay https://ptaff.ca/miguel/
# date            :14 octobre 2020
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


# Entrée: CSV avec les colonnes suivantes:
# 1- Date format 'YYYY-MM-DD'
# 2- Cas quotidien
# 3- Cumulatif des cas
# 4- Rétablis quotidien
# 5- Rétablis totaux
# 6- Décès quotidien
# 7- Décès totaux
# 8- Décès CHSLD quotidien 
# 9- Décès CHSLD totaux

EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
 echo "Usage: `basename $0` qc_total.csv"
 exit $E_BADARGS
fi

source static_wikicode.sh

FICHIER_CSV=$1

HEADER_DECES_QUOTIDIEN="${HEADER}
| yAxisTitle=Décès
| y1Title=Décès quotidien
| y2Title=Décès quotidien CHSLD
| type=line
| legend=Légende"

HEADER_DECES_TOTAUX="${HEADER}
| yAxisTitle=Décès
| y1Title=Totaux
| y2Title=CHSLD
| type=area
| legend=Légende"
## CHSLD
HEADER_DECES_QUOTIDIEN_CHSLD="${HEADER}
| y1Title=Décès quotidien en CHSLD
| type=line"
# HEADER_DECES_TOTAUX_CHSLD="${HEADER_DECES}
# | y1Title=Décès quotidien en CHSLD
# | type=line"



nbr_ligne=`wc -l $FICHIER_CSV | awk '{print $1}'`

# Les données débutent à partir de la 5e ligne. 
lDateISO=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $1","}' | xargs`
lDecesQuotidien=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $6","}' | xargs`
lDecesQuotidienCHSLD=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $8","}' | xargs`
lDecesTotaux=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $7","}' | xargs`
lDecesTotauxCHSLD=`sed -n '3,$p' $FICHIER_CSV| awk  -F "," '{print $9","}' | xargs`

echo "== Décès =="
echo ""
echo "=== Décès quotidiens ==="
echo ""
sWicodeDecesQuotidien="${HEADER_DECES_QUOTIDIEN}
| x=${lDateISO}
| y1=${lDecesQuotidien}
| y2=${lDecesQuotidienCHSLD}
${FOOTER}"
echo "$sWicodeDecesQuotidien"

echo "=== Décès totaux ==="
echo ""
sWicodeDecesTotaux="${HEADER_DECES_TOTAUX}
| x=${lDateISO}
| y1=${lDecesTotaux}
| y2=${lDecesTotauxCHSLD}
${FOOTER}"
echo "$sWicodeDecesTotaux"

