

#!/usr/bin/env bash 
# title          : cas_wikicode.sh
# description     : Charge le fichier CSV découpé de l'INSPQ qui ne contient que les données pour le 
#                   Québec. Fait les manipulations nécessaires pour appeler créer le wikicode pour
#                    les graphiques de la campagne de vaccination.
# author	  : Miguel Tremblay https://ptaff.ca/miguel/
# date            : 24 mars 2021
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


# Entrée: CSV avec les colonnes suivantes:
# 1: Date
# 2: Doses quotidiennes
# 3: Doses totales
# 4:  % de la population vaccinée

# 1: Date
# 2:  Doses quotidiennes 1ere dose
# 3: Doses quotidiennes 2e dose
# 4: Doses quotidiennes 3e dose
# 5:  Doses quotidiennes 4e dose
# 6: Doses quotidiennes total

# 7 : Doses cumul 1ere dose
# 8 : Doses cumul 2e dose
# 9 : Doses cumul 3e dose
# 10: Doses cumul 4e dose

# 11  : Doses cumulatives totales
# 12:  % de la population vaccinée 1ere dose
# 13:  % de la population vaccinée 2e dose



EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
 echo "Usage: `basename $0` vaccin.csv"
 exit $E_BADARGS
fi

FICHIER_CSV=$1

source static_wikicode.sh

HEADER_VACCIN_QUOT="${HEADER_VACC}
| y1Title=1ere dose
| y2Title=2e dose
| y3Title= 3e dose
| y4Title= 4e dose
| y5Title = Total
| yAxisTitle = Nombre de doses"

HEADER_VACCIN_TOT="${HEADER_VACC}
| y1Title=1ere dose
| y2Title=2e dose
| y3Title= 3e dose
| y4Title= 4e dose
| y5Title= Total
| yAxisTitle = Nombre cumulatif de doses"

HEADER_VACCIN_PROP="${HEADER_VACC}
| y1Title=Vaccination de base complétée
| y2Title=Une dose de rappel parmi la population ayant une vaccination de base complétée au 15 août 2022
|hAnnotatonsLine=100.0
|hAnnotatonsLabel=Population complète
| yAxisTitle = Pourcentage de la population vaccinée  (%)"

lDateISO=`awk -F "," '{print $1","}' $FICHIER_CSV | xargs`
# Doses quotidiennes
lDoseQuot1=`awk -F "," '{print $2","}' $FICHIER_CSV | xargs`
lDoseQuot2=`awk -F "," '{print $3","}' $FICHIER_CSV | xargs`
lDoseQuot3=`awk -F "," '{print $4","}' $FICHIER_CSV | xargs`
lDoseQuot4=`awk -F "," '{print $5","}' $FICHIER_CSV | xargs`
lDoseQuotTot=`awk -F "," '{print $6","}' $FICHIER_CSV | xargs`
# Doses totales
lDoseTot1=`awk -F "," '{print $7","}' $FICHIER_CSV | xargs`
lDoseTot2=`awk -F "," '{print $8","}' $FICHIER_CSV | xargs`
lDoseTot3=`awk -F "," '{print $9","}' $FICHIER_CSV | xargs`
lDoseTot4=`awk -F "," '{print $10","}' $FICHIER_CSV | xargs`
lDoseTot=`awk -F "," '{print $11","}' $FICHIER_CSV | xargs`
# Proportion de la pop vaccinée
lDoseProp1=`awk -F "," '{print $12","}' $FICHIER_CSV | xargs`
lDoseProp2=`awk -F "," '{print $13","}' $FICHIER_CSV | xargs`

echo "== Graphiques =="
echo ""
echo "=== Doses quotidiennes  ==="
echo ""
sWikicode="${HEADER_VACCIN_QUOT}
| x=${lDateISO}
| y1=$lDoseQuot1
| y2=$lDoseQuot2
| y3=$lDoseQuot3
| y4=$lDoseQuot4
| y5=$lDoseQuotTot
${FOOTER_VACC}"
echo "$sWikicode"

echo "=== Doses totales  ==="
echo ""
 sWikicode="${HEADER_VACCIN_TOT}
 | x=${lDateISO}
 | y1=$lDoseTot1
 | y2=$lDoseTot2
 | y3=$lDoseTot3
 | y4=$lDoseTot4
 | y5=$lDoseTot
 ${FOOTER_VACC}"
 echo "$sWikicode"

echo "=== Pourcentage de la population vaccinée==="
echo ""
 sWikicode="${HEADER_VACCIN_PROP}
 | x=${lDateISO}
 | y1=$lDoseProp1
 | y2=$lDoseProp2
 ${FOOTER_VACC}"
 echo "$sWikicode"
