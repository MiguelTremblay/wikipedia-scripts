#!/usr/bin/env bash 
# title          : cas_wikicode.sh
# description     : Charge le fichier CSV découpé de l'INSPQ qui ne contient que les données pour le 
#                   Québec. Fait les manipulations nécessaires pour appeler créer le wikicode pour
#                    les graphiques des hosptalisations.
# author	  : Miguel Tremblay https://ptaff.ca/miguel/
# date            :17 octobre 2020
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


# Entrée: CSV avec les colonnes suivantes:
# 1: Date
# 2: hospitalisations (nouvelle définition)
# 3: hospitalisations (ancienne définition)
# 4: soins intensifs
# 5: nombre de tests


EXPECTED_ARGS=2
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
 echo "Usage: `basename $0` hospitalisations.csv qc_total.csv"
 exit $E_BADARGS
fi

FICHIER_CSV_HOSP=$1
FICHIER_CSV_QC_TOTAL=$2


source static_wikicode.sh

HEADER_HOSP="${HEADER}
| yAxisTitle = Personnes 
| yTitle=Hospitalisations"


HEADER_INT="${HEADER}
| yAxisTitle = Personnes 
| yTitle=Soins intensifs"

nbr_ligne=`wc -l $FICHIER_CSV_HOSP | awk '{print $1}'`

# Les données débutent à partir de la 2e ligne. 
## Il faut prendre les premières dates dans le fichier des hospitalisations, car au début de la pandémie il n'y a pas de
## données pour chaque jour. Le reste doit être pris dans le nouveau fichier, car les dates sont parfois 
## désynchronisées entre les deux fichiers
lDateISO1=`sed -n '2,85p' $FICHIER_CSV_HOSP | awk  -F "," '{print $1}' | awk -F "/" '{print $3 "-" $2 "-" $1","}'| xargs`
## Format des dates dans le fichier est dd/mm/yyyy et il faut le remettre comme YYYY-MM-DD
lDateISO2=`sed -n '122,$p' $FICHIER_CSV_QC_TOTAL | awk  -F "," '{print $1","}'| xargs`
lDateISO="${lDateISO1}${lDateISO2}"
# Ancienne formule pour les hospitalisation jusqu'au 2020-05-19
lHOSP_ANCIEN=`sed -n '2,83p' $FICHIER_CSV_HOSP | awk  -F "," '{print $3","}' | xargs`
lHOSP_NOUVEAU=`sed -n '122,$p' $FICHIER_CSV_QC_TOTAL | awk  -F "," '{print $10","}' | sed 's/\.//g' | xargs`
lHOSP="${lHOSP_ANCIEN}${lHOSP_NOUVEAU}"
lSOIN_INTENSIFS_ANCIEN=`sed -n '2,83p' $FICHIER_CSV_HOSP | awk  -F "," '{print $4","}' | xargs | sed ':a;s/, ,/,0,/g;ta'` 
lSOIN_INTENSIFS_NOUVEAU=`sed -n '122,$p' $FICHIER_CSV_QC_TOTAL | awk  -F "," '{print $11","}' |xargs | sed ':a;s/, ,/,0,/g;ta' | sed ':a;s/, \.,/, ,/g;ta'` 
lSOIN_INTENSIFS="${lSOIN_INTENSIFS_ANCIEN}${lSOIN_INTENSIFS_NOUVEAU}"

echo "== Personnes hospitalisées ou aux soins intensifs =="
echo ""
echo "=== Hospitalisation hors soins intensifs ==="
echo ""
sWikicode="${HEADER_HOSP}
| x=${lDateISO}
| y=$lHOSP
${FOOTER_HOSP}"
echo "$sWikicode"

echo "=== Soins intensifs ==="
echo ""
sWikicode="${HEADER_INT}
| x=${lDateISO}
| y=0$lSOIN_INTENSIFS
${FOOTER_SI}"
echo "$sWikicode"
