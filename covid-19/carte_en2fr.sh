#!/usr/bin/env bash

# Applies data taken from ${DATA_SOURCE} to ${DATA_TEMPLATE}
# and saves resulting document in ${DESTINATION}.

# Assumes data lines are all starting with:
# {{Image label small|

DATA_SOURCE='carte_en.txt'
DATA_TEMPLATE='carte_fr.txt'
DESTINATION='carte_out.txt'

# Overwrite ${DESTINATION} with ${DATA_TEMPLATE};
# the ${DESTINATION} will be modified in place for
# each change.
cp -f "${DATA_TEMPLATE}" "${DESTINATION}"

# Find all data lines from ${DATA_SOURCE}, extract values
# and replace them in the ${DESTINATION}.
# We match using the `x` and `y` values as they are the
# only consistent values between source and template.
while read -r; do
	# Extract coordinates for the data point.
	x=$( sed -r 's/^.+\|x=([0-9\.]+)\|.+$/\1/' <<<"${REPLY}" )
	y=$( sed -r 's/^.+\|y=([0-9\.]+)\|.+$/\1/' <<<"${REPLY}" )
	# Extract value for the data point.  The first numeric value
	# following `width|400` is assumed to be the value we want.
	val=$(
		sed -r 's/^.+width\|400.+\|([0-9,]+)\].+$/\1/' <<<"${REPLY}" | \
			`# English uses commas we do not care about; remove them.` \
			sed 's/,//g'
	)
	# If the value we got is numeric, proceed; some coordinates
	# have no value, such as Atlantic Ocean so we skip them.
	if [[ "${val}" =~ ^[0-9]+$ ]]; then
		# Replace the above-read value when the coordinates match.
		sed -ri \
			-e 's/^(\{\{Image label small\|x='"${x}"'\|y='"${y}"'\|.+width\|400\}.+\|)([0-9,]+)([]}].+)$/\1'"${val}"'\3/' \
			"${DESTINATION}"
	fi
done < <( grep '^{{Image label small' < "${DATA_SOURCE}" )

