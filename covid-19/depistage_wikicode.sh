#!/usr/bin/env bash 
# title          : cas_wikicode.sh
# description     : Charge le fichier CSV découpé de l'INSPQ qui ne contient que les données pour le 
#                   Québec. Fait les manipulations nécessaires pour appeler créer le wikicode pour
#                    les graphiques du dépistage.
# author	  : Miguel Tremblay https://ptaff.ca/miguel/
# date            :18 octobre 2020
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


# Entrée: CSV avec les colonnes suivantes:
# 1: Date
# 2: hospitalisations (nouvelle définition)
# 3: hospitalisations (ancienne définition)
# 4: soins intensifs
# 5: nombre de tests


EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
 echo "Usage: `basename $0` hospitalisations.csv"
 exit $E_BADARGS
fi

FICHIER_CSV=$1

source static_wikicode.sh

HEADER_DEP="${HEADER}
| yAxisTitle = Nombre de tests
| y1Title=Nombre de tests de dépistage"

nbr_ligne=`wc -l $FICHIER_CSV | awk '{print $1}'`

# Les données débutent à partir de la 5e ligne. 
## Format des dates dans le fichier est dd/mm/yyyy et il faut le remettre comme YYYY-MM-DD
lDateISO=`sed -n '2,$p' $FICHIER_CSV | awk  -F "," '{print $1}' | awk -F "/" '{print $3 "-" $2 "-" $1","}'| xargs`
lDEPISTAGE=`sed -n '2,$p' $FICHIER_CSV | awk  -F "," '{print $5","}' | xargs`

echo "== Nombre d'analyses de dépistage de la Covid-19 au Québec selon la date d'analyse du prélèvement =="
echo ""
sWikicode="${HEADER_DEP}
| x=${lDateISO}
| y=$lDEPISTAGE
${FOOTER_HOSP}"
echo "$sWikicode"
